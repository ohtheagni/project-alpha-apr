from django.urls import path
from tasks.views import create_task, ShowMyTasksView

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", ShowMyTasksView.as_view(), name="show_my_tasks"),
]
