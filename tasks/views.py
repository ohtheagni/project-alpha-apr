from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskModelForm
from .models import Task
from django.views.generic import ListView


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskModelForm(request.POST)
        if form.is_valid():
            form.save
            return redirect("list_projects")
    else:
        form = TaskModelForm()
    return render(request, "tasks/create_task.html", {"form": form})


class ShowMyTasksView(ListView):
    model = Task
    template_name = "tasks/show_my_tasks.html"
    context_object_name = "my_tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
