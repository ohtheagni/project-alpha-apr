from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectModelForm


# Create your views here.
# AKA Project model
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/project_list.html", context)


def home(request):
    return redirect("list_projects")


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    form = ProjectModelForm()
    if request.method == "POST":
        form = ProjectModelForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
        else:
            form = ProjectModelForm()
    return render(request, "projects/create_project.html", {"form": form})
