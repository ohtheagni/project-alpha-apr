from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User


# Create your views here.
def login_view(request):
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            if (
                form.cleaned_data["password"]
                == form.cleaned_data["password_confirmation"]
            ):
                user = authenticate(
                    username=form.cleaned_data["username"],
                    password=form.cleaned_data["password"],
                )
                if user is None:
                    user = User.objects.create_user(
                        username=form.cleaned_data["username"],
                        password=form.cleaned_data["password"],
                    )
                    login(request, user)
                    return redirect("list_projects")
                else:
                    form.add_error(
                        "username", "This username is already taken"
                    )
            else:
                form.add_error(None, "The passwords do not match")
    else:
        form = SignupForm()
    return render(request, "accounts/signup.html", {"form": form})
